package org.noureddine.joularjx;

import com.github.marschall.memoryfilesystem.MemoryFileSystemBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.noureddine.joularjx.sensor.EnergySensor;
import org.noureddine.joularjx.sensor.EnergySensorType;
import org.noureddine.joularjx.sensor.RpiModel;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PlatformSupportTest {

    private String oldOsName;

    private String oldOsArch;

    @BeforeEach
    void init() {
        oldOsName = System.getProperty("os.name");
        oldOsArch = System.getProperty("os.arch");
    }

    @AfterEach
    void reset() {
        System.setProperty("os.name", oldOsName);
        System.setProperty("os.arch", oldOsArch);
    }

    @Nested
    class Windows {

        private FileSystem fs;

        @BeforeEach
        void initFileSystem() throws IOException {
            fs = MemoryFileSystemBuilder.newWindows().build();
        }

        @AfterEach
        void closeFileSystem() throws IOException {
            fs.close();
        }

        @Test
        void windows10() {
            System.setProperty("os.name", "Windows 10");

            Optional<EnergySensor> platform = PlatformSupport.getPlatform(fs);

            assertTrue(platform.isPresent());
            final EnergySensor sensor = platform.get();
            assertEquals(EnergySensorType.WINDOWS, sensor.getSensor());
            assertNull(sensor.getRpiModel());
        }
    }


    @Nested
    class Linux {

        private FileSystem fs;

        @BeforeEach
        void initFileSystem() throws IOException {
            fs = MemoryFileSystemBuilder.newLinux().build();
        }

        @AfterEach
        void closeFileSystem() throws IOException {
            fs.close();
        }

        @Test
        void amd64RaplSupported() throws IOException {
            System.setProperty("os.name", "Linux");
            System.setProperty("os.arch", "amd64");
            Files.createDirectories(fs.getPath("/sys/class/powercap/intel-rapl/intel-rapl:0"));

            Optional<EnergySensor> platform = PlatformSupport.getPlatform(fs);

            assertTrue(platform.isPresent());
            final EnergySensor sensor = platform.get();
            assertEquals(EnergySensorType.RAPL, sensor.getSensor());
            assertNull(sensor.getRpiModel());
        }

        @Test
        void amd64RaplNotSupported() {
            System.setProperty("os.name", "Linux");
            System.setProperty("os.arch", "amd64");

            Optional<EnergySensor> platform = PlatformSupport.getPlatform(fs);

            assertTrue(platform.isEmpty());
        }

        @ParameterizedTest
        @ValueSource(strings = {"aarch64", "arm"})
        void armNoRpi(final String osArch) {
            System.setProperty("os.name", "Linux");
            System.setProperty("os.arch", osArch);

            Optional<EnergySensor> platform = PlatformSupport.getPlatform(fs);

            assertTrue(platform.isEmpty());
        }

        @ParameterizedTest
        @EnumSource(value = RpiModel.class, names = {"RPI_400", "RPI_4_B_1_2_AARCH64", "RPI_4_B_1_1_AARCH64"}, mode = EnumSource.Mode.INCLUDE)
        void rpiAarch64(final RpiModel rpiModel) throws IOException {
            System.setProperty("os.name", "Linux");
            System.setProperty("os.arch", "aarch64");
            Path deviceTreeModel = fs.getPath("/proc/device-tree/model");
            Files.createDirectories(deviceTreeModel.getParent());
            Files.writeString(deviceTreeModel, rpiModel.getDeviceName(), StandardOpenOption.CREATE_NEW);

            Optional<EnergySensor> platform = PlatformSupport.getPlatform(fs);

            assertTrue(platform.isPresent());
            final EnergySensor sensor = platform.get();
            assertEquals(EnergySensorType.RASPBERRY, sensor.getSensor());
            assertEquals(rpiModel, sensor.getRpiModel());
        }

        @ParameterizedTest
        @EnumSource(value = RpiModel.class, names = {"RPI_400", "RPI_4_B_1_2_AARCH64", "RPI_4_B_1_1_AARCH64"}, mode = EnumSource.Mode.EXCLUDE)
        void rpiArm(final RpiModel rpiModel) throws IOException {
            System.setProperty("os.name", "Linux");
            System.setProperty("os.arch", "arm");
            Path deviceTreeModel = fs.getPath("/proc/device-tree/model");
            Files.createDirectories(deviceTreeModel.getParent());
            Files.writeString(deviceTreeModel, rpiModel.getDeviceName(), StandardOpenOption.CREATE_NEW);

            Optional<EnergySensor> platform = PlatformSupport.getPlatform(fs);

            assertTrue(platform.isPresent());
            final EnergySensor sensor = platform.get();
            assertEquals(EnergySensorType.RASPBERRY, sensor.getSensor());
            assertEquals(rpiModel, sensor.getRpiModel());
        }
    }

    @Nested
    class Mac {

        private FileSystem fs;

        @BeforeEach
        void initFileSystem() throws IOException {
            fs = MemoryFileSystemBuilder.newMacOs().build();
        }

        @AfterEach
        void closeFileSystem() throws IOException {
            fs.close();
        }

        @Test
        void macOsX() {
            System.setProperty("os.name", "Mac OS X");

            Optional<EnergySensor> platform = PlatformSupport.getPlatform(fs);

            assertTrue(platform.isEmpty());
        }
    }
}