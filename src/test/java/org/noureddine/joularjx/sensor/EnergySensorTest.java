package org.noureddine.joularjx.sensor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

class EnergySensorTest {

    @Test
    void raspberrySensorWithRpiModel() {
        assertDoesNotThrow(() -> new EnergySensor(EnergySensorType.RASPBERRY, RpiModel.RPI_3_B_1_2));
    }

    @Test
    void windowsSensorWithRpiModel() {
        assertThrowsExactly(IllegalArgumentException.class, () -> new EnergySensor(EnergySensorType.WINDOWS, RpiModel.RPI_400));
    }

    @Test
    void nullSensorWithRpiModel (){
        assertThrowsExactly(IllegalArgumentException.class, () -> new EnergySensor(null, RpiModel.RPI_400));
    }
}