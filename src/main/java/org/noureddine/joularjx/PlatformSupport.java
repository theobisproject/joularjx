package org.noureddine.joularjx;

import org.noureddine.joularjx.sensor.EnergySensor;
import org.noureddine.joularjx.sensor.EnergySensorType;
import org.noureddine.joularjx.sensor.RpiModel;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

public class PlatformSupport {

    private static final String RAPL_FOLDER = "/sys/class/powercap/intel-rapl/intel-rapl:0";

    private static final String DEVICE_TREE_PATH = "/proc/device-tree/model";

    private static final String OS_LINUX = "linux";

    private static final String OS_WINDOWS = "win";

    private static final String ARCHITECTURE_AARCH64 = "aarch64";

    private static final String ARCHITECTURE_ARM = "arm";

    private PlatformSupport() {

    }

    public static Optional<EnergySensor> getPlatform(final FileSystem fileSystem) {
        // Get OS
        final String osName = System.getProperty("os.name").toLowerCase();
        final String osArch = System.getProperty("os.arch").toLowerCase();

        if (osName.contains(OS_LINUX)) {
            // GNU/Linux
            if (osArch.contains(ARCHITECTURE_AARCH64) || osArch.contains(ARCHITECTURE_ARM)) {
                // Check if Raspberry Pi and use formulas
                final RpiModel raspberryPiModel = getRPiModelName(osArch, fileSystem);
                if (raspberryPiModel != null) {
                    return Optional.of(new EnergySensor(EnergySensorType.RASPBERRY, raspberryPiModel));
                } else {
                    // Platform not supported
                    return Optional.empty();
                }
            } else {
                // Suppose it's x86/64, check for powercap RAPL
                try {
                    if (Files.exists(fileSystem.getPath(RAPL_FOLDER))) {
                        // Rapl is supported
                        return Optional.of(new EnergySensor(EnergySensorType.RAPL));
                    } else {
                        // If no RAPL, then no support
                        return Optional.empty();
                    }
                } catch (Exception e) {
                    // If no RAPL, then no support
                    return Optional.empty();
                }
            }
        } else if (osName.contains(OS_WINDOWS)) {
            // Windows
            // Check for Intel Power Gadget, and PowerJoular Windows
            return Optional.of(new EnergySensor(EnergySensorType.WINDOWS));
        } else {
            // Other platforms not supported
            return Optional.empty();
        }
    }

    /**
     * Get model name of Raspberry Pi
     *
     * @param osArch OS Architecture (arm, aarch64)
     * @return Raspberry Pi model name
     */
    private static RpiModel getRPiModelName(final String osArch, final FileSystem fileSystem) {
        final Path deviceTreeModel = fileSystem.getPath(DEVICE_TREE_PATH);

        if (Files.exists(deviceTreeModel)) {
            try {
                // Read only first line of stat file
                // We need to read values at index 1, 2, 3 and 4 (assuming index starts at 0)
                // Example of line: cpu  83141 56 28074 2909632 3452 10196 3416 0 0 0
                // Split the first line over spaces to get each column
                List<String> allLines = Files.readAllLines(deviceTreeModel);
                for (String currentLine : allLines) {
                    if (currentLine.contains(RpiModel.RPI_400.getDeviceName())) {
                        if (osArch.contains(ARCHITECTURE_AARCH64)) {
                            return RpiModel.RPI_400;
                        }
                    }
                    if (currentLine.contains(RpiModel.RPI_4_B_1_2_AARCH64.getDeviceName())) {
                        if (osArch.contains(ARCHITECTURE_AARCH64)) {
                            return RpiModel.RPI_4_B_1_2_AARCH64;
                        } else {
                            return RpiModel.RPI_4_B_1_2_AARCH32;
                        }
                    } else if (currentLine.contains(RpiModel.RPI_4_B_1_1_AARCH64.getDeviceName())) {
                        if (osArch.contains(ARCHITECTURE_AARCH64)) {
                            return RpiModel.RPI_4_B_1_1_AARCH64;
                        } else {
                            return RpiModel.RPI_4_B_1_1_AARCH32;
                        }
                    } else if (currentLine.contains(RpiModel.RPI_3_B_1_3.getDeviceName())) {
                        return RpiModel.RPI_3_B_1_3;
                    } else if (currentLine.contains(RpiModel.RPI_3_B_1_2.getDeviceName())) {
                        return RpiModel.RPI_3_B_1_2;
                    } else if (currentLine.contains(RpiModel.RPI_2_B_1_1.getDeviceName())) {
                        return RpiModel.RPI_2_B_1_1;
                    } else if (currentLine.contains(RpiModel.RPI_1_B_1_2.getDeviceName())) {
                        return RpiModel.RPI_1_B_1_2;
                    } else if (currentLine.contains(RpiModel.RPI_1_B_2.getDeviceName())) {
                        return RpiModel.RPI_1_B_2;
                    } else if (currentLine.contains(RpiModel.RPI_ZERO_W_1_1.getDeviceName())) {
                        return RpiModel.RPI_ZERO_W_1_1;
                    }
                }
            } catch (IOException ignored) {
            }
        }

        return null;
    }
}
