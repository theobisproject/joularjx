package org.noureddine.joularjx.sensor;

public class EnergySensor {

    private final EnergySensorType sensor;

    private final RpiModel rpiModel;

    public EnergySensor(final EnergySensorType sensor) {
        this(sensor, null);
    }

    public EnergySensor(final EnergySensorType sensor, final RpiModel rpiModel) {
        if (sensor != EnergySensorType.RASPBERRY && rpiModel != null) {
            throw new IllegalArgumentException("RpiModel can only be set with raspberry as sensor!");
        }
        this.sensor = sensor;
        this.rpiModel = rpiModel;
    }

    public EnergySensorType getSensor() {
        return sensor;
    }

    public RpiModel getRpiModel() {
        return rpiModel;
    }
}
