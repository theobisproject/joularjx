package org.noureddine.joularjx.sensor;

public enum RpiModel {

    RPI_400("rbp4001.0-64", "Raspberry Pi 400 Rev 1.0"),
    RPI_4_B_1_2_AARCH64("rbp4b1.2-64", "Raspberry Pi 4 Model B Rev 1.2"),
    RPI_4_B_1_2_AARCH32("rbp4b1.2", "Raspberry Pi 4 Model B Rev 1.2"),
    RPI_4_B_1_1_AARCH64("rbp4b1.1-64", "Raspberry Pi 4 Model B Rev 1.1"),
    RPI_4_B_1_1_AARCH32("rbp4b1.1", "Raspberry Pi 4 Model B Rev 1.1"),
    RPI_3_B_1_3("rbp3b+1.3", "Raspberry Pi 3 Model B Plus Rev 1.3"),
    RPI_3_B_1_2("rbp3b1.2", "Raspberry Pi 3 Model B Rev 1.2"),
    RPI_2_B_1_1("rbp2b1.1", "Raspberry Pi 2 Model B Rev 1.1"),
    RPI_1_B_1_2("rbp1b+1.2", "Raspberry Pi Model B Plus Rev 1.2"),
    RPI_1_B_2("rbp1b2", "Raspberry Pi Model B Rev 2"),
    RPI_ZERO_W_1_1("rbpzw1.1", "Raspberry Pi Zero W Rev 1.1");

    private final String shortName;
    private final String deviceName;

    RpiModel(final String shortName, final String deviceName) {
        this.shortName = shortName;
        this.deviceName = deviceName;
    }

    public String getShortName() {
        return shortName;
    }

    public String getDeviceName() {
        return deviceName;
    }
}
